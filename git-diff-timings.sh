#!/bin/sh
n=100

a=8617be5f55b0bac9282fc755228a32651eae58a1
b=34e4102951c102210404bd4418fdeb10002730de

while [ $n -ge 1 ]; do
    n=$(( $n - 1 ))

#      git diff-tree -z --no-renames --name-only $a: $b: -- >/dev/null
#      git diff-tree -z --no-renames $a: $b: -- >/dev/null

    # git cat-file tree $a: >/dev/null
    # git cat-file tree $b: >/dev/null

    git cat-file -t $a: >/dev/null
    git cat-file -t $b: >/dev/null

    git ls-tree -z $a: >/dev/null
    git ls-tree -z $b: >/dev/null

done

